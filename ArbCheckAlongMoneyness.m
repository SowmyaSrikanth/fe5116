% This function checks arbitrage of call price along moneyness
% Input
%   fwdCurve - forward Curve
%   Ks - vector of strikes
%   T1 - time to expiry 1
%   T2 - time to expiry 2
%   vol1 - vector of volatilities at time T1
%   vol2 - vector of volatilities at time T2

function arbCheckAlongMoneyness(fwdCurve, Ks, T1, T2, vol1, vol2)

    for j = 1: length(Ks)
       % get forward Spot price for the tenor. G(T1)
       fwd = getFwdSpot(fwdCurve, T1);
       % if K = forward price, check arbitrage. 
       if Ks(j) == fwd
          % C(K,T1)
          CallT1 = getBlackCall(fwd, T1, Ks(j), vol1(j));
          % G(T2)
          fwdT2 = getFwdSpot(fwdCurve, T2);
          % K*G(T2)/G(T1)
          KT2 = (Ks(j)*fwdT2)/fwd;
          % C(K*G(T2)/G(T1), T2)
          CallT2 = getBlackCall(fwd, T2, KT2, vol2(j)); 
          if CallT1 > CallT2
              error("ArbCheckAlongMoneyness:Arb_Detected", "ArbCheckAlongMoneyness: Arbitrage detected! Price of a call option is not increasing along moneyness lines");
          end
       end
    end
   