classdef ArbCheckAlongMoneynessTest < matlab.unittest.TestCase
    properties
       init = TestInitialize.initialize(); 
    end
    methods (Test)      
        function testIsArbDetectedAlongMoneyness(testCase)
           Ks = [1.4 0.5 1.65];
           T1 = 1;
           T2 = 1.34;
           vol1 = [0.23 0.34 0.56];
           vol2 = [0.24 0.43 0.35];
           testCase.init.fwdCurve.rates(366) = 1.4; 
           testCase.verifyError(@() arbCheckAlongMoneyness(testCase.init.fwdCurve, Ks, T1, T2, vol1, vol2), 'ArbCheckAlongMoneyness:Arb_Detected');
        end
    end
end