classdef ArbCheckTest < matlab.unittest.TestCase
    methods (Test) 
        function testIsCallPriceNegative(testCase)
           T = 0.12;
           Ks = [1.5 1.6 1.7 1.4 1.8];
           f = 0.47;
           vols = [0.208 0.202 0.2 0.204 0.216];
           testCase.verifyError(@() arbCheck(f, T, Ks, vols), "arbCheck:Call_not_positive");
        end
        function testIsCallPriceMonotonicallyDecreasing(testCase)
           T = 0.12;
           Ks = [0.54 2.0 1.2 1.65 1.65];
           f = 1.523;
           vols = [0.208 0.202 0.2 0.204 0.216];
           testCase.verifyError(@() arbCheck(f, T, Ks, vols), "arbCheck:Call_not_monotonically_decr_K");
        end
        function testIsCallPriceConvex(testCase)
           T = 0.12;
           Ks = [1.5 1.7 1.3 1.1 1.9];
           f = 1.63;
           vols = [0.208 0.202 0.2 0.204 0.216];
           testCase.verifyError(@() arbCheck(f, T, Ks, vols), "arbCheck:Call_not_convex");
        end
    end
end