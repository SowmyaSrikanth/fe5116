classdef GetBlackCallTest < matlab.unittest.TestCase
    methods (Test)        
        function testCallOptionEqualsFwdWhenStrikeIsZero(testCase)
            f = 1.343;
            T = 0.12;
            Vs = [0.208 0.202 0.2 0.204 0.216];
            res = getBlackCall(f, T , zeros(1,5), Vs);
            testCase.verifyEqual(res, [f f f f f]);
        end
        function testTimeToExpiryIsNonZero(testCase)
            f = 1.343;
            Ks = [1.3 1.45 1.37 1.46 1.56];
            Vs = [0.208 0.202 0.2 0.204 0.216];
            testCase.assertError(@() getBlackCall(f, 0, Ks, Vs), 'getBlackCall:Invalid_Input');
        end
        function testFwdPriceIsDouble(testCase)
            f = '1.234';
            T = 0.12;
            Ks = [1.3 1.45 1.37 1.46 1.56];
            Vs = [0.208 0.202 0.2 0.204 0.216];
            testCase.assertError(@() getBlackCall(f, T, Ks, Vs), 'getBlackCall:Invalid_Input');
        end
        function testFwdPriceIsNonZero(testCase)
            f = 0;
            T = 0.12;
            Ks = [1.3 1.45 1.37 1.46 1.56];
            Vs = [0.208 0.202 0.2 0.204 0.216];
            testCase.assertError(@() getBlackCall(f, T, Ks, Vs), 'getBlackCall:Invalid_Input');
        end
        function testVolsIsNonZero(testCase)
            f = 1.43;
            T = 0.12;
            Ks = [1.3 1.45 1.37 1.46 1.56];
            testCase.assertError(@() getBlackCall(f, T, Ks, zeros(1,5)), 'getBlackCall:Invalid_Input');
        end
        function testVolsStrikesVectorMatch(testCase)
            f = 1.43;
            T = 0.12;
            Ks = [1.3 1.45 1.46 1.56];
            Vs = [0.208 0.202 0.2 0.204 0.216];
            testCase.assertError(@() getBlackCall(f, T, Ks, Vs), 'getBlackCall:Mismatch_Dimension');
        end
    end
end

 