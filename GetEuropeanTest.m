classdef GetEuropeanTest < matlab.unittest.TestCase
    properties 
       init = TestInitialize.initialize(); 
    end
    methods (Test)      
        function testEuropeanCallEqualToBS(testCase)
           fwd = getFwdSpot(testCase.init.fwdCurve, 1);
           vol = getVol(testCase.init.volSurface, 1, fwd);
           call =  getEuropean(testCase.init.volSurface, 1, @(x)max(x-fwd,0));
           bs_call = getBlackCall(fwd, 1, fwd, vol(end));
           testCase.verifyEqual(call, bs_call, 'AbsTol', 1e-04);
        end
        function testEuropeanPutEqualToBS(testCase)
           fwd = getFwdSpot(testCase.init.fwdCurve, 1);
           vol = getVol(testCase.init.volSurface, 1, fwd);
           put =  getEuropean(testCase.init.volSurface, 1, @(x)max(fwd - x,0));
           bs_put = getBlackCall(fwd, 1, fwd, vol(end));
           testCase.verifyEqual(put, bs_put, 'AbsTol', 1e-04);
        end
        function testTimeToExpiryIsDouble(testCase)
            testCase.assertError(@() getEuropean(testCase.init.volSurface, '1', @(x)max(x-fwd,0)), 'getEuropean:Invalid_Input');
        end
        function testTimeToExpiryIsZero(testCase)
            testCase.assertError(@() getEuropean(testCase.init.volSurface, 0, @(x)max(x-fwd,0)), 'getBlackCall:Invalid_Input');
        end
        function testFwdCurveExists(testCase)
            testCase.init.volSurface = rmfield(testCase.init.volSurface, 'fwdCurve');
            testCase.assertError(@() getEuropean(testCase.init.volSurface, 1, @(x)max(x-fwd,0)), 'getEuropean:Invalid_Input');
        end
        function testTenorExists(testCase)
            testCase.init.volSurface = rmfield(testCase.init.volSurface, 'Ts');
            testCase.assertError(@() getEuropean(testCase.init.volSurface, 1, @(x)max(x-fwd,0)), 'getEuropean:Invalid_Input');
        end
        function testIntervalsWithoutZeroAndInf(testCase)
           testCase.assertError(@() getEuropean(testCase.init.volSurface, 1, @(x)max(x-1.45,0), [2, 3, Inf]), 'getEuropean:Invalid_Input');
        end
        function testIntervalsHasTwoIntervals(testCase)
           testCase.assertError(@() getEuropean(testCase.init.volSurface, 1, @(x)max(x-1.45,0), [Inf]), 'getEuropean:Invalid_Input');
        end
        function testWithDuplicateIntervals(testCase)
           testCase.verifyWarning(@() getEuropean(testCase.init.volSurface, 1, @(x)max(x-1.45,0), [0 0 Inf]), ""); 
        end
    end
end

function u = getBlackPut (f, Ts, Ks, Vs)
   
   assert(isa(f, 'double'), "getBlackPut:Invalid_Input", "getBlackPut: Forward price should be of type double");   
   assert(Ts > 0, "getBlackPut:Invalid_Input", "getBlackPut: Time to expiry cannot less than or equal to 0");
   
   stdDev = Vs .* sqrt(Ts);
   
   % f / Ks
   fwdKs = zeros(size(Ks));
   
   for i = 1:length(Ks)
       assert(stdDev(i) > 0, "getBlackPut:Invalid_Input", "getBlackPut: Standard Deviation cannot be less than or equal to 0");
       % When Strike is 0, dividing by 0 is invalid 
       if (Ks(i) == 0)
           % Setting to Infinity to get call price as forward price
           fwdKs(i) = Inf;
       else
           fwdKs(i) = f / Ks(i);
            % log(0) does not exist and log of negative numbers results in complex numbers.   
           assert(fwdKs(i) > 0, "getBlackPut:Invalid_Input", "getBlackPut: Forward price/Strike cannot be less than or equal to 0.");
       end
   end
   
   d1 = log(fwdKs)./stdDev + (stdDev.*0.5);
   d2 = d1 - stdDev;

   u = Ks .* normcdf (-d2) - f .* normcdf (-d1);
   
end