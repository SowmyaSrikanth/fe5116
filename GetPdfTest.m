classdef GetPdfTest < matlab.unittest.TestCase
    properties
       data = getInitialData()
    end
    methods (Test)        
        function testPdfIsPositive(testCase)
            pdf = getPdf(testCase.data.init.volSurface, testCase.data.init.Ts(end),testCase.data.smile.Ks);
            testCase.verifyGreaterThanOrEqual(all(pdf),0);
        end 
        function testSumEqualOne(testCase)
            pdf = getPdf(testCase.data.init.volSurface, 1, (0:0.01:15));
            testCase.verifyEqual(sum(pdf.*0.01),1, 'AbsTol', 1e-07);
        end
        function testMeanIsFwd(testCase)
            fwd = getFwdSpot(testCase.data.init.fwdCurve, 1);
            pdf = getPdf(testCase.data.init.volSurface, 1, [0:0.01:15]);
%             testCase.verifyEqual(mean(pdf.*fwd/100),fwd);
            testCase.verifyEqual(sum(pdf.*[0:0.01:15]/100), fwd, 'AbsTol', 1e-07);
        end
        function testTimeToExpiryIsDouble(testCase)
            testCase.assertError(@() getPdf(testCase.data.init.volSurface, '1', 1.43), 'getPdf:Invalid_Input');
        end
        function testErrorOnTimeToExpiryBeyondLastTenor(testCase)
            testCase.verifyError(@() getPdf(testCase.data.init.volSurface, testCase.data.init.volSurface.Ts(end)+0.5, 1.43), 'getVol:Invalid_Input');
        end
        function testEmptyStrikeVector(testCase)
            testCase.assertError(@() getPdf(testCase.data.init.volSurface, 1, []), 'getPdf:Invalid_Input');
        end
        function testFwdCurveExists(testCase)
            testCase.data.init.volSurface = rmfield(testCase.data.init.volSurface, 'fwdCurve');
            testCase.assertError(@() getPdf(testCase.data.init.volSurface, 1, [1.43, 1.43]), 'getPdf:Invalid_Input');
        end
        function testTenorExists(testCase)
            testCase.data.init.volSurface = rmfield(testCase.data.init.volSurface, 'Ts');
            testCase.assertError(@() getPdf(testCase.data.init.volSurface, 1, [1.43, 1.43]), 'getPdf:Invalid_Input');
        end
        function testSmileExists(testCase)
            testCase.data.init.volSurface = rmfield(testCase.data.init.volSurface, 'smile');
            testCase.assertError(@() getPdf(testCase.data.init.volSurface, 1, [1.43, 1.43]), 'getPdf:Invalid_Input');
        end
        function testSmileCurveExists(testCase)
            testCase.data.init.volSurface = rmfield(testCase.data.init.volSurface.smile, 'curve');
            testCase.assertError(@() getPdf(testCase.data.init.volSurface, 1, [1.43, 1.43]), 'getPdf:Invalid_Input');
        end
        
    end
end

function data = getInitialData()
    data.init = TestInitialize.initialize();
    data.smile = makeSmile(data.init.fwdCurve, data.init.Ts(end), data.init.cps, data.init.deltas, data.init.vols(end,:));
end