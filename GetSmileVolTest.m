classdef GetSmileVolTest < matlab.unittest.TestCase  
    properties 
       data = getInitialData();
    end
    methods (Test)        
        function testVolsReturnedNSupportVector(testCase)
             expectedVols = testCase.data.init.vols(end,:);
             realVols = getSmileVol(testCase.data.smile, testCase.data.smile.Ks);
             testCase.verifyEqual(expectedVols, realVols);
         end
         function testVolsAllNonNegative(testCase)
             realVols = getSmileVol(testCase.data.smile, testCase.data.smile.Ks);
             testCase.verifyGreaterThanOrEqual(realVols, 0);
         end
         function testStrikeShouldNotBeNegative(testCase)
             testCase.assertError(@() getSmileVol(testCase.data.smile, (-1)), "getSmileVol:Invalid_Input");
         end
         function testStrikeShouldBeDouble(testCase)
             testCase.assertError(@() getSmileVol(testCase.data.smile, 'test'), "getSmileVol:Invalid_Input");
         end
         function testAllCoefficientAvailable(testCase)
             testCase.data.smile.abs = testCase.data.smile.abs(1:3);
             testCase.assertError(@() getSmileVol(testCase.data.smile, testCase.data.smile.Ks), "getSmileVol:Invalid_Input");
         end
         function testStrikeCanBeSingleValue(testCase)
             realVol = getSmileVol(testCase.data.smile, testCase.data.smile.Ks(end));
             expectedVol = testCase.data.init.vols(end);
             testCase.verifyEqual(realVol, expectedVol);
         end  
         function testIsMakeSmileGenerated(testCase)
             testCase.assertError(@() makeSmile(testCase.data.init.fwdCurve, testCase.data.init.Ts(end), testCase.data.init.cps, testCase.data.init.deltas, testCase.data.init.vols), "makeSmile:Mismatch_Dimension");
         end
    end
end

function data = getInitialData()
    data.init = TestInitialize.initialize();
    data.smile = makeSmile(data.init.fwdCurve, data.init.Ts(end), data.init.cps, data.init.deltas, data.init.vols(end,:));
end

