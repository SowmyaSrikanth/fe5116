classdef GetVolTest < matlab.unittest.TestCase
    properties 
       init = TestInitialize.initialize(); 
    end
    methods (Test)   
        function testVolFromVolSurfaceMatchesMarketData(testCase)
           fwd = getFwdSpot(testCase.init.fwdCurve, testCase.init.Ts(end));
           strike = getStrikeFromDelta(fwd, testCase.init.Ts(end), testCase.init.cps(3), testCase.init.vols(end,3), testCase.init.deltas(3));
           vol = getVol(testCase.init.volSurface, testCase.init.Ts(end), strike);
           testCase.verifyEqual(vol, testCase.init.vols(end,3), 'AbsTol', 1e-12);
        end
        function testFwdPriceFromVolSurfaceMatchesFwdSpot(testCase)
           fwd = getFwdSpot(testCase.init.fwdCurve, testCase.init.Ts(end));
           strike = getStrikeFromDelta(fwd, testCase.init.Ts(end), testCase.init.cps(3), testCase.init.vols(end,3), testCase.init.deltas(3));
           [~, f] = getVol(testCase.init.volSurface, testCase.init.Ts(end), strike);
           testCase.verifyEqual(f, fwd, 'AbsTol', 1e-12);
        end
        function testTimeToExpiryIsDouble(testCase)
            testCase.assertError(@() getVol(testCase.init.volSurface, '1', 1.43), 'getVol:Invalid_Input');
        end
        function testVolAtTimeToExpiryZero(testCase)
            vols = getVol(testCase.init.volSurface, 0, 1.43);
            testCase.verifyEqual(vols, 0);
        end
        function testFwdAtTimeToExpiryZero(testCase)
            [~, f] = getVol(testCase.init.volSurface, 0, 1.43);
            testCase.verifyEqual(f, 0);
        end
        function testErrorOnTimeToExpiryBeyondLastTenor(testCase)
            testCase.verifyError(@() getVol(testCase.init.volSurface, testCase.init.volSurface.Ts(end)+0.5, 1.43), 'getVol:Invalid_Input');
        end
        function testEmptyStrikeVector(testCase)
            testCase.assertError(@() getVol(testCase.init.volSurface, 1, []), 'getVol:Invalid_Input');
        end
        function testFwdCurveExists(testCase)
            testCase.init.volSurface = rmfield(testCase.init.volSurface, 'fwdCurve');
            testCase.assertError(@() getVol(testCase.init.volSurface, 1, [1.43, 1.43]), 'getVol:Invalid_Input');
        end
        function testTenorExists(testCase)
            testCase.init.volSurface = rmfield(testCase.init.volSurface, 'Ts');
            testCase.assertError(@() getVol(testCase.init.volSurface, 1, [1.43, 1.43]), 'getVol:Invalid_Input');
        end
        function testSmileExists(testCase)
            testCase.init.volSurface = rmfield(testCase.init.volSurface, 'smile');
            testCase.assertError(@() getVol(testCase.init.volSurface, 1, [1.43, 1.43]), 'getVol:Invalid_Input');
        end
        function testSmileCurveExists(testCase)
            testCase.init.volSurface = rmfield(testCase.init.volSurface.smile, 'curve');
            testCase.assertError(@() getVol(testCase.init.volSurface, 1, [1.43, 1.43]), 'getVol:Invalid_Input');
        end
    end
end