function RunTests()

    runtests('GetBlackCallTest');
    runtests('GetRateIntegralTest');
    runtests('GetFwdSpotTest');
    runtests('GetStrikeFromDeltaTest');
    runtests('GetSmileVolTest');
    runtests('GetVolTest');
    runtests('ArbCheckTest');
    runtests('ArbCheckAlongMoneynessTest');
    runtests('GetPdfTest');
    runtests('GetEuropeanTest');
    
    