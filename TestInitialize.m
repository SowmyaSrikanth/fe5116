classdef TestInitialize
    properties
        init;
    end
   methods(Static)
       function init = initialize()
            [spot, lag, days, domdfs, fordfs, vols, cps, deltas] = getMarket();
            init.Ts = days/365;
            init.spot = spot;
            init.domdfs = domdfs;
            init.fordfs = fordfs;
            init.domCurve = makeDepoCurve(init.Ts, domdfs);
            init.forCurve = makeDepoCurve(init.Ts, fordfs);
            init.tau = lag/365;
            init.fwdCurve = makeFwdCurve(init.domCurve, init.forCurve, spot, init.tau);
            init.cps = cps;
            init.deltas = deltas;
            init.vols = vols;
            init.volSurface = makeVolSurface(init.fwdCurve, init.Ts, init.cps, init.deltas, init.vols);
       end
   end
end