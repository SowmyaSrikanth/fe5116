% This function performs three arbitrage checks
% Input:
%   fwd - forward spot rate
%   T - time to expiry
%   Ks - vector of strikes
%   vols - vector of volatilities

function arbCheck(fwd, T, Ks, vols)

    % The price of a call option must be positive: i.e. for any K it must
    % be C(K) > 0. Use eps here as it is close to zero. 
    
    Cs  = getBlackCall(fwd, T, Ks, vols);
    if any(Cs <= eps)
        error('arbCheck:Call_not_positive','Arbitrage detected! Price of call option not positive for all K');
    end
    
    K_with_zero = [0, Ks];
    C_with_zero = [fwd,Cs];
    
    % The price of a call option must be monotonically decreasing in strike, 
    % but it must not decrease too fast: for any consecutive pair of strikes (Ki,Ki+1) 
    % the price of an undiscounted call option must be decreasing
    
    for i= 1:length(Cs)-1
        if (Cs(i+1)-Cs(i))/(Ks(i+1)-Ks(i)) <= -1 || (Cs(i+1)-Cs(i))/(Ks(i+1)-Ks(i)) > 0
            error('arbCheck:Call_not_monotonically_decr_K','Arbitrage detected! Price of a call option does not monotonically decrease with strike');
        end
    end    

    % The price of a call option must be convex in strike: 
    % For any consecutive triplet of strikes (Ki-1,Ki,Ki+1), 
    % the gradient of the price of an undiscounted call option must be increasing

    for i= 2:length(C_with_zero)-1
        if (C_with_zero(i) - C_with_zero(i-1))/(K_with_zero(i) - K_with_zero(i-1)) >= (C_with_zero(i+1) - C_with_zero(i))/(K_with_zero(i+1) - K_with_zero(i))   
            error('arbCheck:Call_not_convex','Arbitrage detected! Price of call option is not convex in strike');
        end
    end