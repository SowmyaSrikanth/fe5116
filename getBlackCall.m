% Inputs :
%   f: forward spot for time T, i.e. E[S(T)]
%   T: time to expiry of the option
%   Ks: vector of strikes
%   Vs: vector of implied Black volatilities
% Output :
%   u: vector of call options undiscounted prices
                    
function u = getBlackCall (f, Ts, Ks, Vs)
   
   assert(isa(f, 'double'), "getBlackCall:Invalid_Input", "getBlackCall: Forward price should be of type double");   
   assert(Ts > 0, "getBlackCall:Invalid_Input", "getBlackCall: Time to expiry cannot less than or equal to 0");
   assert(length(Ks) == length(Vs), "getBlackCall:Mismatch_Dimension", "getBlackCall: Mismatch in dimensions of strike vector and volatility vector");
   
   stdDev = Vs .* sqrt(Ts);
   
   % f / Ks
   fwdKs = zeros(size(Ks));
   
   for i = 1:length(Ks)
       assert(stdDev(i) > 0, "getBlackCall:Invalid_Input", "getBlackCall: Standard Deviation cannot be less than or equal to 0");
       % When Strike is 0, dividing by 0 is invalid 
       if (Ks(i) == 0)
           % Setting to Infinity to get call price as forward price
           fwdKs(i) = Inf;
       else
           fwdKs(i) = f / Ks(i);
            % log(0) does not exist and log of negative numbers results in complex numbers.   
           assert(fwdKs(i) > 0, "getBlackCall:Invalid_Input", "getBlackCall: Forward price/Strike cannot be less than or equal to 0.");
       end
   end
   
   d1 = log(fwdKs)./stdDev + (stdDev.*0.5);
   d2 = d1 - stdDev;

   u = f .* normcdf (d1) - Ks .* normcdf (d2);
      
                         
                         