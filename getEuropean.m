% Computes the price of a European payoff by integration
% Input :
%   volSurface : volatility surface data
%   T : time to maturity of the option
%   payoff : payoff function
%   ints : optional , repartition of integration intervals e.g. [0, 3, +Inf]
% Output :
%   u : forward price of the option ( undiscounted price )

function u = getEuropean(volSurface, T, payoff, varargin)

    assert(isa(T, 'double') && T >= 0, 'getEuropean:Invalid_Input', "getEuropean: Time to expiry should be of type double");
    assert(isfield(volSurface, 'fwdCurve'), 'getEuropean:Invalid_Input', "getEuropean: VolSurface struct is missing forward curve data");
    assert(isfield(volSurface, 'Ts'), 'getEuropean:Invalid_Input', "getEuropean: VolSurface struct is missing tenor data");
    assert(isfield(volSurface, 'smile'), 'getEuropean:Invalid_Input', "getEuropean: VolSurface struct is missing smile data");
    assert(isfield(volSurface.smile, 'curve'), 'getEuropean:Invalid_Input', "getEuropean: VolSurface struct is missing smile curve data");
    assert(isa(payoff, 'function_handle'), "getEuropean:Invalid_Input", "getEuropean: Payoff function is missing");
        
    % optional variable ints
    ints = [];  

    if nargin > 3 
       % assign optional variable to ints
       assert(numel(varargin{1}) >=2, "getEuropean:Invalid_Input", "getEuropean: There must be atleast two intervals");
       ints = varargin{1}; 
       assert(ismember(0, ints) && ismember(Inf, ints), "getEuropean:Invalid_Input", "getEuropean: Intervals must contain 0 and Inf");
       
       % Checks for duplicates in intervals. Removes duplicates and
       % continues processing after displaying warning. 
       if length(ints) ~= length(unique(ints))
          warning('getEuropean: There are duplicate values present in intervals. Duplicates has been removed to continue pricing');
          ints = unique(ints);
       end
    end

    continue_to_integrate = true;
    interval_length = 5;  
    % increment size
    increment = 0.01;  
    area = 0;
    loop_num = 1;

    while(continue_to_integrate)        
        start_value = (loop_num - 1) * interval_length;
        end_value =  loop_num * interval_length;
        % As array cannot be set to infinite size, we process in intervals to get pdf values
        x = (start_value:increment:end_value);  
        
        % To remove discontinuty points between 0 and Inf
        for i= 2 : length(ints) - 1
            % Iterate to next discounuity point if the point is not in this range of x values
            if ints(i) < start_value || ints(i) > end_value
                break;
            end
            point = find(abs(x-ints(i)) < increment);
            if length(point) == 1
                % When x is at discontuity point, assign a slightly smaller value just beside x 
                x(point) = ints(i) - eps;
            end
        end

        pdf = getPdf(volSurface, T, x);
        pdfs = payoff(x) .* pdf;
        % compute area under x values
        sum_area = trapz(x, pdfs);    
        area = area + sum_area;    
        
        % to ensure integral accuracy
        if sum_area < eps
            continue_to_integrate = false;
        end
        
        loop_num = loop_num + 1;
    end
    
    u = area;








