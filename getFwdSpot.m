% Inputs: 
%   curve: pre-computed fwd curve data 
%   T: forward spot date
% Output: 
%   fwdSpot: E[S(t) | S(0)] 

function fwdSpot = getFwdSpot(curve, T)
    assert(isfield(curve, 'rates'), "getFwdSpot:Invalid_Input", "getFwdSpot: Curve struct is missing forward rates data");
    assert(isfield(curve, 'dates'), "getFwdSpot:Invalid_Input", "getFwdSpot: Curve struct is missing forward dates data");
    assert(length(curve.dates) == length(curve.rates), "getFwdSpot:Mismatch_Dimension", "getFwdSpot: Mismatch in array size of rates and dates");
    assert(isa(T, 'double'), "getFwdSpot:Invalid_Input", "getFwdSpot: Forward spot date should be of type double");
    
    T_in_days = round(T*365);
    assert(curve.dates(end) >= T_in_days && T >= 0, "getFwdSpot:Invalid_Input", sprintf("getFwdSpot: Time value entered is invalid. Enter time between %s and %s years", string(1/365), string(curve.dates(end)/365)));
    
    % S(T) = G(T), where forward spot price is rate at time T in forward curve.
    % Add 1 to T_in_days as first element starts from Day 0
    fwdSpot = curve.rates(T_in_days + 1,1);

