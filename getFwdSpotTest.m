classdef GetFwdSpotTest < matlab.unittest.TestCase
    properties 
       init = TestInitialize.initialize(); 
    end
    methods (Test) 
        function testFwdSpotDateIsDouble(testCase)            
            testCase.assertError(@() getFwdSpot(testCase.init.fwdCurve, '2'), "getFwdSpot:Invalid_Input");
        end
        function testFwdSpotDateIsValid(testCase)
            testCase.assertError(@() getFwdSpot(testCase.init.fwdCurve, 10), "getFwdSpot:Invalid_Input");
        end
        function testFwdSpotDateAtZeroIsSpot(testCase)
            fwd = getFwdSpot(testCase.init.fwdCurve, 0);
            testCase.verifyEqual(fwd, testCase.init.spot);
        end
        function testFwdCurveValidity(testCase)
            testCase.init.fwdCurve.rates = testCase.init.fwdCurve.rates(1:45);
            testCase.init.fwdCurve.dates = testCase.init.fwdCurve.dates(1:30);
            testCase.assertError(@() getFwdSpot(testCase.init.fwdCurve, 1), "getFwdSpot:Mismatch_Dimension");
        end
        function testFwdCurveIsValid(testCase)
            testCase.init.fwdCurve = rmfield(testCase.init.fwdCurve, 'rates');
            testCase.assertError(@() getFwdSpot(testCase.init.fwdCurve, 1), "getFwdSpot:Invalid_Input");
        end
    end
end


