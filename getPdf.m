% Compute the probability density function of the price S(T)
% Inputs :
%   volSurf : volatility surface data
%   T: time to expiry of the option
%   Ks: vector of strikes
% Output :
%   pdf: probablity distribution function

function pdf = getPdf(volSurf, T, Ks)

    assert(isa(T, 'double') && T >= 0, 'getPdf:Invalid_Input', "getPdf: Time to expiry should be of type double");
    assert(~isempty(Ks), 'getPdf:Invalid_Input', "getPdf: Vector of strikes cannot be empty");
    assert(isfield(volSurf, 'fwdCurve'), 'getPdf:Invalid_Input', "getPdf: volSurf struct is missing forward curve data");
    assert(isfield(volSurf, 'Ts'), 'getPdf:Invalid_Input', "getPdf: volSurf struct is missing tenor data");
    assert(isfield(volSurf, 'smile'), 'getPdf:Invalid_Input', "getPdf: volSurf struct is missing smile data");
    assert(isfield(volSurf.smile, 'curve'), 'getPdf:Invalid_Input', "getPdf: volSurf struct is missing smile curve data");
    
    % Choose range of Ks for interpolation based on strikes input.
    K = (0:0.1:max(max(Ks)+1,15));
    
    [vols,fwd] = getVol(volSurf, T, K);

    Cs = getBlackCall(fwd, T, K, vols);

    % cubic spline for strike and call price. 
    pp = csape (K, Cs, 'variational');
    pp.coefs;
     
    pdf = zeros(1, length(Ks));
    for i = 1 : length(Ks)
        if floor(Ks(i)*10) >= length(K) - 1
            j = length(K) - 1;
        else
            j = floor(Ks(i)*10)+1;
        end
		% second order derivation coefficients. 
        pdf(i) = 6 *  pp.coefs(j,1) * Ks(i) + 2 * pp.coefs(j,2) - 6 * pp.coefs(j,1) * pp.breaks(j);
    end
