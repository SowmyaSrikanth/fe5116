% Inputs :
%   curve : pre - computed data about an interest rate curve
%   t: time
% Output :
%   integ : integral of the local rate function from 0 to t

function integ = getRateIntegral (curve, t)
    assert(isfield(curve, 'rates'), "getRateIntegral:Invalid_Input", "getRateIntegral: Curve struct is missing rates data");
    assert(isfield(curve, 'dates'), "getRateIntegral:Invalid_Input", "getRateIntegral: Curve struct is missing dates data");
    assert(length(curve.dates) == length(curve.rates), "getRateIntegral:Mismatch_Dimension", "getRateIntegral: Mismatch in array size of rates and dates");
    
    assert(isa(t, 'double'), "getRateIntegral:Invalid_Input", "getRateIntegral: Time should be of type double");
    % round to closest integer. 
    t_in_days = round(t*365);
    lastTenor = curve.dates(end);
    assert(lastTenor >= t_in_days && t > 0, "getRateIntegral:Invalid_Input", sprintf("getRateIntegral: Time value entered is invalid. Enter time between %s and %s", string(1/365), string(lastTenor/365)));

    y = curve.rates;    
    % Calculte area under curve from time 1 to t.
    integ = sum(y(1:t_in_days)./365);
    