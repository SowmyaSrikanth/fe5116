classdef GetRateIntegralTest < matlab.unittest.TestCase
    properties 
       init = TestInitialize.initialize(); 
    end
    methods (Test)  
        function CalculateDomCurveDiscountFactorFromZeroRates(testCase)
            dfs = zeros(size(testCase.init.Ts));
            for i = 1:length(testCase.init.Ts)
               dfs(i) = exp(-getRateIntegral(testCase.init.domCurve, testCase.init.Ts(i)));
            end
            testCase.verifyEqual(dfs, testCase.init.domdfs, 'AbsTol', 1e-6);
        end
        function CalculateForCurveDiscountFactorFromZeroRates(testCase)
            dfs = zeros(size(testCase.init.Ts));
            for i = 1:length(testCase.init.Ts)
               dfs(i) = exp(-getRateIntegral(testCase.init.forCurve, testCase.init.Ts(i)));
            end
            testCase.verifyEqual(dfs, testCase.init.fordfs, 'AbsTol', 1e-6);
        end
        function testDomCurveIsValid(testCase)
            testCase.assertError(@() getRateIntegral(1, 1), "getRateIntegral:Invalid_Input");
        end
        function testForCurveIsValid(testCase)
            testCase.assertError(@() getRateIntegral(2, 1), "getRateIntegral:Invalid_Input");
        end
        function testDomCurveTenorIsValid(testCase)
            testCase.assertError(@() getRateIntegral(testCase.init.domCurve, 7), "getRateIntegral:Invalid_Input");
        end
        function testForCurveTenorIsValid(testCase)
            testCase.assertError(@() getRateIntegral(testCase.init.forCurve, 7), "getRateIntegral:Invalid_Input");
        end
        function testDomCurveTenorIsDouble(testCase)
            testCase.assertError(@() getRateIntegral(testCase.init.domCurve, '1'), "getRateIntegral:Invalid_Input");
        end
        function testForCurveTenorIsDouble(testCase)
            testCase.assertError(@() getRateIntegral(testCase.init.forCurve, '1'), "getRateIntegral:Invalid_Input");
        end
        function testDomCurveTenorIsNonZero(testCase)
            testCase.assertError(@() getRateIntegral(testCase.init.domCurve, 0), "getRateIntegral:Invalid_Input");
        end
        function testForCurveTenorIsNonZero(testCase)
            testCase.assertError(@() getRateIntegral(testCase.init.forCurve, 0), "getRateIntegral:Invalid_Input");
        end
        function testDomCurveValidity(testCase)
            testCase.init.domCurve.rates = testCase.init.domCurve.rates(1:45);
            testCase.init.domCurve.dates = testCase.init.domCurve.dates(1:30);
            testCase.assertError(@() getRateIntegral(testCase.init.domCurve, 1), "getRateIntegral:Mismatch_Dimension");
        end
        function testForCurveValidity(testCase)      
            testCase.init.forCurve.rates = testCase.init.forCurve.rates(1:45);
            testCase.init.forCurve.dates = testCase.init.forCurve.dates(1:30);
            testCase.assertError(@() getRateIntegral(testCase.init.forCurve, 1), "getRateIntegral:Mismatch_Dimension");
        end
    end
end
