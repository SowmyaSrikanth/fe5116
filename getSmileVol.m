% Inputs :
%   curve : pre - computed smile data
%   Ks: vetor of strikes
% Output :
%   vols : implied volatility at strikes Ks

function vols = getSmileVol (curve , Ks)
    assert(isfield(curve, 'abs'), "getSmileVol:Invalid_Input", "getSmileVol: Curve struct is missing spline coefficients data");
    assert(length(curve.abs) == 4, "getSmileVol:Invalid_Input", "getSmileVol: Missing spline coefficient");
    assert(isa(curve.abs, 'double'), "getSmileVol:Invalid_Input", "getSmileVol: Curve coefficients should be of type - double")
    assert(isa(Ks, 'double'), "getSmileVol:Invalid_Input", "getSmileVol: K should be of type double or vector of double");
    assert(all(Ks(:) >= 0), "getSmileVol:Invalid_Input", "getSmileVol: Strikes should not be negative");
    
    % Spline coefficients
    aL = curve.abs (1);
    bL = curve.abs (2);
    aR = curve.abs (3);
    bR = curve.abs (4);
    pp = curve.spline;
    
    vols = zeros(size(Ks));
    
    for i = 1 : length (Ks)
        if Ks(i) <= curve.spline.breaks(1)
            vols (i) = fnval(pp,pp.breaks(1)) + aL * tanh (bL * (pp.breaks(1)- Ks(i)));
        elseif Ks(i) <= curve.spline.breaks(end)
            vols (i) = fnval(pp,Ks(i));
        else
            vols (i) = fnval(pp,pp.breaks(end)) + aR * tanh (bR * ( Ks (i) -pp.breaks (end)));
        end
    end