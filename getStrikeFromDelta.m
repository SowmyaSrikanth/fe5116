% Inputs :
%   fwd: forward spot for time T, i.e. E[S(T)]
%   T: time to expiry of the option
%   cp: 1 for call , -1 for put
%   sigma : implied Black volatility of the option
%   delta : delta in absolute value (e.g. 0.25)
% Output :
%   K: strike of the option

function K = getStrikeFromDelta (fwd, T, cp, sigma, delta)
   assert(isa(cp, 'double') && (cp == 1 || cp == -1), "getStrikeFromDelta:Invalid_Input", "getStrikeFromDelta: Call/Put vector contains invalid inputs. Valid inputs are 1 for call and -1 for put");
   assert(isa(sigma, 'double') && sigma > 0, "getStrikeFromDelta:Invalid_Input", "getStrikeFromDelta: Implied Black volatility value is not of type - double or greater than 0");
   assert(isa(delta, 'double') && delta > 0, "getStrikeFromDelta:Invalid_Input", "getStrikeFromDelta: Delta in absolute value is not of type - double or greater than 0");
   assert(isa(fwd, 'double') && fwd > 0, "getStrikeFromDelta:Invalid_Input", "getStrikeFromDelta: Forward spot price is not of type - double or greater than 0");
   assert(isa(T,'double') && T > 0, "getStrikeFromDelta:Invalid_Input", "getStrikeFromDelta: Time to expiry of option is not of type - double or greater than 0");

   % Calculate d1 value by taking inverse of normal distribution.
   % Calculation varies for Call and Put Option
   if cp == -1
       delta = 1 - delta;
   end
      
   d1 = norminv(delta);
   
   % Using root search to find K value. 
   % Here, we equate the below equation to zero. 
   Func = @(x) getD1(fwd, x, T, sigma) - d1;
   
   % The Levenberg-Marquardt algorithm, uses a search direction that is a
   % cross between the Gauss-Newton direction and the steepest descent
   % direction. The initial point = 0.001.
   options = optimoptions('fsolve', 'Algorithm', 'levenberg-marquardt','Display','off');
   [K, ~] = fsolve(Func, 0.001, options);
   
    
% Calculate d1 using formula.    
function d1 = getD1(fwd, K, T, sigma)
    sigmaSqrT = sigma * sqrt(T);
    d1 = (log(fwd/K) / sigmaSqrT)  + 0.5*sigmaSqrT;
    
    
    