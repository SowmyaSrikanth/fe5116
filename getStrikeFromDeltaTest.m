classdef GetStrikeFromDeltaTest < matlab.unittest.TestCase
    methods (Test)        
        function testRootSearchResultValid(testCase)
           fwd = 1.34;
           T = 1;
           cp = -1;
           vol = 0.202;
           delta = 0.25;
           rootSearch = getStrikeFromDelta(fwd, T, cp, vol, delta);
           d1 = norminv(1 - delta);
           stdDev = vol * sqrt(T);
           K = exp(log(fwd) - (d1 - (0.5 *stdDev)) * stdDev);
           testCase.verifyEqual(rootSearch, K, 'AbsTol', 1e-06);
        end
        function testFwdSpotIsDouble(testCase)
           testCase.assertError(@() getStrikeFromDelta('1.24', 1, -1, 0.202, 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testFwdSpotIsNonZero(testCase)
           testCase.assertError(@() getStrikeFromDelta(0, 1, -1, 0.202, 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testTimeToExpiryIsNonZero(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, 0, -1, 0.202, 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testTimeToExpiryIsDouble(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, "1", -1, 0.202, 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testCallPutIsValid(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, 1, 2, 0.202, 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testCallPutIsDouble(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, 1, "2", 0.202, 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testVolatilityIsDouble(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, 1, 1, "0.202", 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testVolatilityIsNonZero(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, 1, 1, 0, 0.25), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testDeltaIsDouble(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, 1, 1, 0.202, "0.25"), "getStrikeFromDelta:Invalid_Input"); 
        end
        function testDeltaIsNonZero(testCase)
           testCase.assertError(@() getStrikeFromDelta(1.24, 1, 1, 0.202, 0), "getStrikeFromDelta:Invalid_Input"); 
        end
    end
end