% Inputs :
%   volSurf : volatility surface data
%   T: time to expiry of the option
%   Ks: vector of strike
% Output :
%   vols : volatilities
%   fwd: forward spot price for maturity T

function [vols , fwd] = getVol (volSurf, T, Ks)

    assert(isa(T, 'double') && T >= 0, 'getVol:Invalid_Input', "getVol: Time to expiry should be of type double");
    assert(~isempty(Ks), 'getVol:Invalid_Input', "getVol: Vector of strikes cannot be empty");
    assert(isfield(volSurf, 'fwdCurve'), 'getVol:Invalid_Input', "getVol: volSurf struct is missing forward curve data");
    assert(isfield(volSurf, 'Ts'), 'getVol:Invalid_Input', "getVol: volSurf struct is missing tenor data");
    assert(isfield(volSurf, 'smile'), 'getVol:Invalid_Input', "getVol: volSurf struct is missing smile data");
    assert(isfield(volSurf.smile, 'curve'), 'getVol:Invalid_Input', "getVol: volSurf struct is missing smile curve data");
    
    vols = zeros(1,length(Ks));
    
    % At T = 0, volatility and forward are 0.
    if T == 0
       fwd = 0;
       return;
    end

    if T > volSurf.Ts(end)
         error('getVol:Invalid_Input', "gelVol:Volatility cannot be calculated beyond last tenor of %s years", string(volSurf.Ts(end)));
    end
    
    % Forward price
    fwd = getFwdSpot(volSurf.fwdCurve, T);
    
    num_tenors = size(volSurf.Ts,1);
    for i = 1: length(Ks)                      
        Moneyness = Ks(i)/fwd;
        if T <= volSurf.Ts(1)
            % K1 derived along Moneyness
            K = Moneyness*getFwdSpot(volSurf.fwdCurve, volSurf.Ts(1));
            % sigma(1)(K1)
            smileVol = getSmileVol(volSurf.smile.curve(1), K);
            vols(i) = smileVol;
            break;
        end

        for j = 1:num_tenors - 1
            if volSurf.Ts(j+1) >= T && T > volSurf.Ts(j)
                % K(i) and K(i+1) derived along moneyness
                K1 = Moneyness*getFwdSpot(volSurf.fwdCurve, volSurf.Ts(j));
                volK1 = getSmileVol(volSurf.smile.curve(j), K1);
                K2 = Moneyness*getFwdSpot(volSurf.fwdCurve, volSurf.Ts(j+1));
                volK2 = getSmileVol(volSurf.smile.curve(j+1), K2);
                denom = volSurf.Ts(j+1) - volSurf.Ts(j);
                vols(i) = sqrt((volK1 * volK1 * volSurf.Ts(j) * (volSurf.Ts(j+1) - T) + volK2 * volK2 * volSurf.Ts(j+1) * (T-volSurf.Ts(j)))/(denom * T));
                break;
            end
        end
    end
