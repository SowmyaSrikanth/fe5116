% Inputs :
%   ts: array of size N containing times to settlement in years
%   dfs: array of size N discount factors
% Output :
%   curve : a struct containing data needed by getRateIntegral

function curve = makeDepoCurve (ts , dfs)
    assert(~isempty(ts), "makeDepoCurve:Invalid_Input", "makeDepoCurve: Vector of tenors cannot be empty");
    assert(ts(1) ~= 0, "makeDepoCurve:Invalid_Input", "makeDepoCurve: Time to settlement cannot be zero");
    assert(length(ts) == length(dfs), "makeDepoCurve:Mismatch_Dimension", "makeDepoCurve: Mismatch in the size of discount factor and tenor data");
  
    fprintf("Interpolating Zero curves for domestic and foreign rates...\n");
    
    zeroRate = zeros(size(ts));
    % convert discount rate to Zero rate curve    
    zeroRate(1) =-log(dfs(1))/ts(1);
    for i = 2 : length(ts)
        diffDfs = dfs(i-1)/dfs(i);
        zeroRate(i) = log(diffDfs)/(ts(i)-ts(i-1));        
    end
    
    % Convert to days
    ts_in_days = ts * 365;
    
    lastTenor = ts_in_days(end);
    settleDate = today('datetime') + 2;
    % Calculate settle dates considering T+2 is settle date
    dates = settleDate + ts_in_days;
   
    % Generate IR zero curve using Annual Compounding, Act/365 Basis and Piecewise constant interpolation method. 
    IntRateCurve = IRDataCurve('Zero', datenum(settleDate), datenum(dates), zeroRate, 'Compounding', 1, 'Basis', 3 , 'InterpMethod', 'constant');
   
    % Interpolating upto 30 days beyond the last tenor. 
    % Assuming interval of 1 day. 
    rates = getZeroRates(IntRateCurve, datenum(settleDate)+1:1:datenum(settleDate)+lastTenor+30, 'Compounding', 1, 'Basis', 3);
    
    curve.rates = rates;
    curve.dates = (1:1:lastTenor+30)';
%     plot(curve.dates, curve.rates)       

    