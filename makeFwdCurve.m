% Inputs:
%   domCurve: domestic IR curve data
%   forCurve: domestic IR curve data
%   spot: spot exchange rate
%   tau: lag between spot and settlement
% Output:
%   curve: a struct containing data needed by getFwdSpot

function curve=makeFwdCurve(domCurve, forCurve, spot, tau)
    assert(isfield(domCurve, 'rates'), "makeFwdCurve:Invalid_Input", "makeFwdCurve: DomCurve struct is missing domestic rates data");
    assert(isfield(domCurve, 'dates'), "makeFwdCurve:Invalid_Input", "makeFwdCurve: DomCurve struct is missing domestic dates data");
    assert(isfield(forCurve, 'rates'), "makeFwdCurve:Invalid_Input", "makeFwdCurve: ForCurve struct is missing foreign rates data");
    assert(isfield(forCurve, 'dates'), "makeFwdCurve:Invalid_Input", "makeFwdCurve: ForCurve struct is missing foreign dates data");
    assert(length(domCurve.rates) == length(forCurve.rates), "makeFwdCurve:Mismatch_Dimension", "makeFwdCurve: Mismatch in size of domestic curve and foreign curve rates data");
    assert(length(domCurve.dates) == length(forCurve.dates), "makeFwdCurve:Mismatch_Dimension", "makeFwdCurve: Mismatch in size of domestic curve and foreign curve dates data");
    assert(isa(spot, 'double'), "makeFwdCurve:Invalid_Input", "makeFwdCurve: Spot price should be of type - double");
    assert(isa(tau, 'double') && tau > 0, "makeFwdCurve:Invalid_Input", "makeFwdCurve: Tau should be of type - double and greater than 0");
    
    fprintf("Creating Forward curve...\n");
    
    tau_in_days = tau*365;
    
    % Difference between domestic and foreign rates --> r(t) - y(t)
    diffCurve.rates = domCurve.rates - forCurve.rates;
    diffCurve.dates = domCurve.dates;
    
    % Spot is 2-days forward price of cash rate
    cashRate = spot * exp(-getRateIntegral(diffCurve, tau));
    
    for j=1:length(diffCurve.dates) - tau_in_days
        % Calculate Integral of difference rate from 0 to t+tau
        diffRate = getRateIntegral(diffCurve, (domCurve.dates(j)/365)+tau);
        curve.rates(j,1) = cashRate * exp(diffRate);
        curve.dates(1,j) = j;
    end
    
    curve.rates = [spot;curve.rates];
    curve.dates = [0,curve.dates];
    
    %plot(curve.dates, curve.rates);









