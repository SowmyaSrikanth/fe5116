% Inputs :
%   fwd: forward spot for time T, i.e. E[S(T)]
%   T: time to expiry of the option
%   cps: vetor if 1 for call , -1 for put
%   deltas : vector of delta in absolute value (e.g. 0.25)
%   vols : vector of volatilities
% Output :
%   Ks: strikes obtained from delta
%   curve : a struct containing data needed in getSmileK

function [ curve ] = makeSmile(fwdCurve, T, cps, deltas, vols)

    % assert vector dimension match
    assert(length(cps) == length(deltas) & length(deltas) == length(vols) & length(cps) == length(vols), "makeSmile:Mismatch_Dimension", "makeSmile: dimensions of Call/Put, Delta and Volatilities does not match");

    fwd = getFwdSpot(fwdCurve, T);

    Ks = zeros(1, length(cps));
    % resolve strikes using getStrikesFromDelta
    for i = 1 : length (cps)
        Ks(i) = getStrikeFromDelta(fwd, T, cps(i), vols(i), deltas(i));
    end
    
    curve.Ks = Ks;

    % Arbitrage Check
    arbCheck(fwd, T, Ks, vols);

    % compute spline coefficients
    pp = csape(Ks, vols, 'variational');
    
    % compute parameters aL , bL , aR and bR
    curve.spline = pp;
    sqRoot = sqrt(0.5);
    K_N = Ks(1,length(Ks(1,:)));
    K_1 = Ks(1,1);
    
    bR = atanh (sqRoot) / ( ( K_N * K_N / Ks(1,length(Ks(1,:))-1) ) - K_N);  
    bL = atanh (sqRoot) / (K_1 - ( K_1 * K_1 / Ks(1,2) ) );
    
    % Calculating aR and aL using syms function is very expensive computationally, hence, manual derivation is used below. 
    
    % compute aL for extrapolation using first order derivative of first section of spline
    aL = (3 * pp.coefs(1,1) *K_1^2 + 2 *(pp.coefs(1,2) - 3 * pp.coefs(1,1) * pp.breaks(1)) * K_1 + 3 * pp.coefs(1,1) * pp.breaks(1)^2 - 2 * pp.coefs(1,2) * pp.breaks(1) + pp.coefs(1,3))/(-bL);
    
    % compute aR for extrapolation using first order derivative of last section of spline
    aR = (3 * pp.coefs(end,1) * K_N^2 + 2 * (pp.coefs(end,2) - 3 * pp.coefs(end,1) * pp.breaks(end)) * K_N + 3 * pp.coefs(end,1) * pp.breaks(end)^2 - 2 * pp.coefs(end,2) * pp.breaks(end) + pp.coefs(end,3))/(-bR);

    % compute aL and aR using syms function
    
    %     syms f(x)
    %     % function for first section of spline
    %     f(x) = pp.coefs(1,1) * ( x - pp.breaks(1))^3 + pp.coefs(1,2) * ( x - pp.breaks(1))^2 + pp.coefs(1,3) * ( x - pp.breaks(1)) + pp.coefs(1,4);
    %     g = diff(f);
    %     % compute aL for extrapolation
    %     aL = double(g(K_1) / (- bL));
    % 
    %     % function for last section of spline
    %     f(x) = pp.coefs(end,1) * ( x - pp.breaks(end))^3 + pp.coefs(end,2) * ( x - pp.breaks(end))^2 + pp.coefs(end,3) * ( x - pp.breaks(end)) + pp.coefs(end,4);
    %     g = diff(f);
    %     % compute aR for extrapolation
    %     aR = double( g(Ks(1,end)) / bR );

    curve.abs = [aL, bL, aR, bR];
