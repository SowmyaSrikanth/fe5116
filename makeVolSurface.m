% Inputs :
%   fwdCurve : forward curve data
%   Ts: vector of expiry times
%   cps: vetor of 1 for call , -1 for put
%   deltas : vector of delta in absolute value (e.g. 0.25)
%   vols : matrix of volatilities
% Output :
%   surface : a struct containing data needed in getVol

function volSurf = makeVolSurface (fwdCurve, Ts, cps, deltas, vols)

    assert(~isempty(Ts), "makeVolSurface:Invalid_Input", "makeVolSurface: Vector of tenors cannot be empty");
    assert(isfield(fwdCurve, 'rates'), "makeVolSurface:Invalid_Input", "makeVolSurface: fwdCurve struct is missing rates data");
    assert(isfield(fwdCurve, 'dates'), "makeVolSurface:Invalid_Input", "makeVolSurface: fwdCurve struct is missing dates data");
    assert(fwdCurve.dates(end) >= round(Ts(end)*365), "makeVolSurface:Invalid_Input", "makeVolSurface: Forward curve does not contain data for all tenors");

    fprintf("Generating Volatility Surface...\n");
    rows = size(Ts,1);
    
    % Create an empty struct for smile
    smile = struct;
    for i = 1:rows        
        % get Smile Curve for each tenor
        smile.curve(i) = makeSmile(fwdCurve, Ts(i), cps, deltas, vols(i,:));
        
        % Ignore last Tenor as Call price cannot be calculated with one tenor.
        if i == rows
            break;
        end
        arbCheckAlongMoneyness(fwdCurve, smile.curve(i).Ks, Ts(i), Ts(i+1), vols(i,:), vols(i+1, :));
    end 
    
    volSurf.smile = smile;
    volSurf.Ts = Ts;
    volSurf.fwdCurve = fwdCurve;
    
    fprintf("Generated Volatility Surface...\n");
    